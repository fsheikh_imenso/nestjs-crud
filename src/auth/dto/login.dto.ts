import { IsEmail, IsNotEmpty, IsString, MinLength } from 'class-validator';

export class CreateLoginDto {
    
    @IsNotEmpty()
    @IsEmail({}, { message: 'Enter correct email' })
    readonly email: string;
  
    @IsString()
    @IsNotEmpty()
    @MinLength(6)
    readonly password: string;
}
