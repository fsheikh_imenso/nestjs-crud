import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Model } from 'mongoose';
import { User } from 'src/user/schemas/user.schemas';
import { CreateLoginDto } from './dto/login.dto';
import * as bcrypt from 'bcryptjs';
import { InjectModel } from '@nestjs/mongoose';
import { UserService } from 'src/user/user.service';

@Injectable()
export class AuthService {
    constructor(
        public userService: UserService,
        private jwtService: JwtService
    ){}

    async login(loginDto: CreateLoginDto): Promise<{ token: string }> {
        const { email, password } = loginDto;
        const user = await this.userService.findOne(email);

        if (!user) {
            throw new UnauthorizedException('Invalid email and password');
        }

        const isPasswordMatch = await bcrypt.compare(password, user.password);
        if (!isPasswordMatch) {
            throw new UnauthorizedException('Invalid email and password');
        }

        const token = this.jwtService.sign({ id: (user as any)._id });
        
        return { token };
    }
}
