import { Body, Controller, Post,Get } from '@nestjs/common';
import { AuthService } from './auth.service';
import { CreateLoginDto } from './dto/login.dto';

@Controller('auth')
export class AuthController {
    constructor(private authService:AuthService){}

    @Post('/login')
    login(@Body() loginDto:CreateLoginDto): Promise<{token:string}>{

        const token = this.authService.login(loginDto);
        
        return token;
    }
    
}
