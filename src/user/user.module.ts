import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from './schemas/user.schemas';

@Module({
  imports:[MongooseModule.forFeature([{name:'User',schema:UserSchema}])],
  controllers: [UserController],
  providers: [UserService,User],
  exports: [UserService,User],
})
export class UserModule {}
