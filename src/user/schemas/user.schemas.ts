import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { IsEmail } from "class-validator";


@Schema({
    timestamps:true
})
export class User{
    @Prop()
    name:string

    @Prop({unique:[true, "Already exist"]})
    email:string

    @Prop()
    password:string
}

export const UserSchema = SchemaFactory.createForClass(User)