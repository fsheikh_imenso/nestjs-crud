import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { InjectModel } from '@nestjs/mongoose';
import { User } from './schemas/user.schemas';
import * as mongoose from 'mongoose';
import * as bcrypt from 'bcryptjs';

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User.name)
    private userModel: mongoose.Model<User>,
  ) {}


 async create(createUserDto: CreateUserDto){
    const {name, email, password} = createUserDto
    const hashPass = await bcrypt.hash(password,10)
    const user = await this.userModel.create({
      name,
      email,
      password:hashPass
    })
  }

  async findAll(): Promise<User[]> {
    const users = await this.userModel.find().exec();
    return users;
  }
  
  update(id: number, updateUserDto: UpdateUserDto) {
    return `This action updates a #${id} user`;
  }

  remove(id: string):Promise<User> {

    return  this.userModel.findByIdAndDelete(id);
  }

  async findOne(email:string): Promise<User | null> {
    return this.userModel.findOne({ email }).exec();
  }

  async findById(_id: string): Promise<User | null> {
    return this.userModel.findOne({ _id }).exec();
  }

  async usertoken(){
   return 'token';
  }
}
