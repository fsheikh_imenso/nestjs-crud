import { Controller, Get, Render } from '@nestjs/common';
import { AppService } from './app.service';
import { Book } from './book/schema/book.schema';
import { BookService } from './book/book.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}
  @Get('login')
  @Render('login') // Render the login.hbs template
  getLogin() {
    return {};
  }

  @Get('admin')
  @Render('admin') // Render the admin.hbs template
  getAdminPanel() {
    // Admin panel data can be passed here if needed
    return {};
  }
}
