import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { BookModule } from './book/book.module';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';

import { BookSchema } from './book/schema/book.schema';
import { UserModule } from './user/user.module';
import { UserSchema } from './user/schemas/user.schemas';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath:'.env',
      isGlobal:true
    }),
    MongooseModule.forRoot(process.env.DB_URI),
    BookModule,    
   MongooseModule.forFeature([{name:'Book',schema:BookSchema}]),
   MongooseModule.forFeature([{name:'User',schema:UserSchema}]),
   UserModule,
   AuthModule,
 
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
