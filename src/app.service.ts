import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Book } from './book/schema/book.schema';
import * as mongoose from 'mongoose';

@Injectable()

export class AppService {
  constructor(
    @InjectModel (Book.name)
    private bookModel: mongoose.Model<Book>,
  ) {}

  getHello(): string {
    return 'Hello World!';
  }

  // async getallbook(): Promise<Book[]> {
  //   const books = await this.bookModel.find().exec();
  //   return books;
  // }
}
