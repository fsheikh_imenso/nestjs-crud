import { IsNotEmpty, IsString } from "class-validator";
import { User } from "src/user/schemas/user.schemas";


export class CreateBookDto{

     @IsNotEmpty()
     @IsString()
     title: string;

     @IsString()
     @IsNotEmpty()
     description: string;


     @IsString()
     @IsNotEmpty()
     author: string;


     @IsNotEmpty()
     price: number;


     @IsString()
     @IsNotEmpty()
     category: String;

    
     user_id: User;

     




}