import { IsNotEmpty, IsString } from "class-validator";

import * as mongoose from 'mongoose';
import { User } from "src/user/schemas/user.schemas";


export class UpdateBookDto{

     
     title: string;

     
     description: string;


     author: string;


  
     price: number;


     category: String;

     user_id: User;

}