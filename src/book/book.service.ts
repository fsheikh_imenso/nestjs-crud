import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Book } from './schema/book.schema';
import * as mongoose from 'mongoose';
import { throwError } from 'rxjs';
import { Query } from 'express-serve-static-core';
import { CreateBookDto } from './data/create-book.dto';

@Injectable()
export class BookService {
  constructor(
    @InjectModel(Book.name)
    private bookModel: mongoose.Model<Book>,
  ) {}



  async getAllBooks(): Promise<{ books: Book[]; count: number; totalPrice:number }> {
    const [books, count] = await Promise.all([
      this.bookModel.find().exec(),
      this.bookModel.countDocuments().exec(),
    ]);
    const totalPrice = books.reduce((sum, book) => sum + book.price, 0);
    return { books, count , totalPrice };
  }



  async findAll(query: Query): Promise<Book[]> {
    const keyword = query.keyword
      ? {
          title: {
            $regex: query.keyword,
            // $option:'i'
          },
        }
      : {};

    const resPerPage = 2;
    const currentPage = Number(query.page) || 1;
    const skip = resPerPage * (currentPage - 1);

    const books = await this.bookModel
      .find({ ...keyword })
      .limit(resPerPage)
      .skip(skip);
    return books;
  }

  async create(book: Book): Promise<Book> {
    console.log('add book');
    const add = await this.bookModel.create(book);

    return add;
  }

  async findById(id: string): Promise<Book> {
    const res = await this.bookModel.findById(id);
    if (!res) {
      throw new NotFoundException('Book not found');
    }
    return res;
  }

  async updateById(id: string, book: Book): Promise<Book> {
    return await this.bookModel.findByIdAndUpdate(id, book, {
      new: true,
      runValidaror: true,
    });
  }



  async deleteById(id: string): Promise<Book> {
    return await this.bookModel.findByIdAndDelete(id);
  }

  async totalPrice(): Promise<any> {
    const total = await this.bookModel.aggregate([
      {
        $group: {
          _id: null,
          totalPrice: { $avg: "$price" },
        },
      },
    ]);

    if (total.length > 0) {
      return total[0].totalPrice;
    } else {
      return 0; // Return 0 if no orders exist
    }
  }

  async sort(): Promise<any[]> {
    return this.bookModel.aggregate([
      {
        $sort: {
          price: 1
        },
      },
    ]);
  }

  async match(): Promise<any[]>
  {
    return this.bookModel.aggregate([{
      $match:{
        category:"crime"
      }
    }])
  }

  async count(): Promise<any[]>
  {
    return this.bookModel.aggregate([
    {
      $count: "books"
    }
  ])
  }

  
  
}
