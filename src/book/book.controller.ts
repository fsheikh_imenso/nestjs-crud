import { Body, Controller, Delete, Get, Param, ParseIntPipe, ParseUUIDPipe, Post, Put, Query, Redirect, Render, Res, UseGuards } from '@nestjs/common';
import { BookService } from './book.service';
import { Book } from './schema/book.schema';
import { CreateBookDto } from './data/create-book.dto';
import { Query as ExpressQuery} from 'express-serve-static-core';
import { get } from 'http';
import { UpdateBookDto } from './data/update-book.dto';
import { Response } from 'express';
import { AuthGuard } from '@nestjs/passport';

@Controller()
export class BookController {
  constructor(private bookService: BookService) {}

  // @UseGuards(AuthGuard())
  @Get()
  @Render('index')
  async getAll() {
    const { books, count, totalPrice } = await this.bookService.getAllBooks();
    return { books, count , totalPrice };
  }

  @Get('/get')
  async getAllBooks(@Query() query:ExpressQuery): Promise<Book[]> {
    return this.bookService.findAll(query);
  }

  @Post('/add')
  @Redirect('/')
  async addBook(@Body() book: CreateBookDto): Promise<Book> {
    return this.bookService.create(book);
  }

  @Post('get/:id') 
   async getBook(@Param('id') id: string): Promise<Book> {
    const editdata = this.bookService.findById(id);
    return editdata;
  }

  @Post('/update/:id')
  @Redirect('/')
  async updateBook(
    @Param('id')
    id: string,
    @Body() book: UpdateBookDto,
  ): Promise<Book> {
    return this.bookService.updateById(id, book);
  }
  

  @Get('/delete/:id')
  @Redirect('/')
  async deleteBook(@Param('id') id: string): Promise<Book> {
    console.log('Deleted Succesfully');
    return this.bookService.deleteById(id);
  }

  @Get('/total-price')
  async totalPrice() {
    return this.bookService.totalPrice();
  }


  @Get('/sort')
  async lookupData(): Promise<any[]> {
    return this.bookService.sort();
  }

  @Get('/match')
  async match(): Promise<any[]> {
    return this.bookService.match();
  }

  @Get('/count')
  async count(): Promise<any[]> {
    return this.bookService.count();
  }
}
