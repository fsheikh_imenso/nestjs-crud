import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

import { Document, Types, SchemaTypes } from 'mongoose';
import { User } from 'src/user/schemas/user.schemas';

export type BookDocument = Book & Document;

@Schema({
  timestamps: true,
})
export class Book {
  @Prop()
  title: string;

  @Prop()
  description: string;

  @Prop()
  author: string;

  @Prop()
  price: number;

  @Prop()
  category: String;

  @Prop({ type: SchemaTypes.ObjectId, ref: User.name })
  user_id: User;
}

export const BookSchema = SchemaFactory.createForClass(Book);
